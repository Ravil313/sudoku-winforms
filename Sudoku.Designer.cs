﻿namespace Sudoku
{
    partial class Sudoku
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sudoku));
            this.Check = new System.Windows.Forms.Button();
            this.Easy = new System.Windows.Forms.Button();
            this.Medium = new System.Windows.Forms.Button();
            this.Hard = new System.Windows.Forms.Button();
            this.Answer = new System.Windows.Forms.Button();
            this.TimerBox = new System.Windows.Forms.TextBox();
            this.PauseBtn = new System.Windows.Forms.Button();
            this.PauseScreen = new System.Windows.Forms.PictureBox();
            this.ContinueBtn = new System.Windows.Forms.Button();
            this.RatingButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PauseScreen)).BeginInit();
            this.SuspendLayout();
            // 
            // Check
            // 
            this.Check.Enabled = false;
            this.Check.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Check.Location = new System.Drawing.Point(474, 159);
            this.Check.Name = "Check";
            this.Check.Size = new System.Drawing.Size(78, 23);
            this.Check.TabIndex = 0;
            this.Check.Text = "Проверить";
            this.Check.UseVisualStyleBackColor = true;
            this.Check.Click += new System.EventHandler(this.Check_Click);
            // 
            // Easy
            // 
            this.Easy.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Easy.Location = new System.Drawing.Point(474, 358);
            this.Easy.Name = "Easy";
            this.Easy.Size = new System.Drawing.Size(78, 23);
            this.Easy.TabIndex = 1;
            this.Easy.Text = "Легкая";
            this.Easy.UseVisualStyleBackColor = true;
            this.Easy.Click += new System.EventHandler(this.Easy_Click);
            // 
            // Medium
            // 
            this.Medium.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Medium.Location = new System.Drawing.Point(474, 387);
            this.Medium.Name = "Medium";
            this.Medium.Size = new System.Drawing.Size(78, 23);
            this.Medium.TabIndex = 2;
            this.Medium.Text = "Средняя";
            this.Medium.UseVisualStyleBackColor = true;
            this.Medium.Click += new System.EventHandler(this.Medium_Click);
            // 
            // Hard
            // 
            this.Hard.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Hard.Location = new System.Drawing.Point(474, 416);
            this.Hard.Name = "Hard";
            this.Hard.Size = new System.Drawing.Size(78, 23);
            this.Hard.TabIndex = 3;
            this.Hard.Text = "Сложная";
            this.Hard.UseVisualStyleBackColor = true;
            this.Hard.Click += new System.EventHandler(this.Hard_Click);
            // 
            // Answer
            // 
            this.Answer.Enabled = false;
            this.Answer.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Answer.Location = new System.Drawing.Point(474, 267);
            this.Answer.Name = "Answer";
            this.Answer.Size = new System.Drawing.Size(78, 23);
            this.Answer.TabIndex = 4;
            this.Answer.Text = "Ответ";
            this.Answer.UseVisualStyleBackColor = true;
            this.Answer.Click += new System.EventHandler(this.Answer_Click);
            // 
            // TimerBox
            // 
            this.TimerBox.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TimerBox.Location = new System.Drawing.Point(474, 12);
            this.TimerBox.Name = "TimerBox";
            this.TimerBox.ReadOnly = true;
            this.TimerBox.Size = new System.Drawing.Size(78, 23);
            this.TimerBox.TabIndex = 5;
            this.TimerBox.Text = "00 : 00 : 00";
            this.TimerBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // PauseBtn
            // 
            this.PauseBtn.Enabled = false;
            this.PauseBtn.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PauseBtn.Location = new System.Drawing.Point(474, 41);
            this.PauseBtn.Name = "PauseBtn";
            this.PauseBtn.Size = new System.Drawing.Size(78, 23);
            this.PauseBtn.TabIndex = 6;
            this.PauseBtn.Text = "Пауза";
            this.PauseBtn.UseVisualStyleBackColor = true;
            this.PauseBtn.Click += new System.EventHandler(this.PauseBtn_Click);
            // 
            // PauseScreen
            // 
            this.PauseScreen.Enabled = false;
            this.PauseScreen.ErrorImage = global::Sudoku.Properties.Resources._125;
            this.PauseScreen.ImageLocation = "";
            this.PauseScreen.InitialImage = global::Sudoku.Properties.Resources._125;
            this.PauseScreen.Location = new System.Drawing.Point(0, 0);
            this.PauseScreen.Name = "PauseScreen";
            this.PauseScreen.Size = new System.Drawing.Size(455, 455);
            this.PauseScreen.TabIndex = 7;
            this.PauseScreen.TabStop = false;
            this.PauseScreen.Visible = false;
            // 
            // ContinueBtn
            // 
            this.ContinueBtn.Enabled = false;
            this.ContinueBtn.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ContinueBtn.Location = new System.Drawing.Point(474, 70);
            this.ContinueBtn.Name = "ContinueBtn";
            this.ContinueBtn.Size = new System.Drawing.Size(78, 23);
            this.ContinueBtn.TabIndex = 8;
            this.ContinueBtn.Text = "Дальше";
            this.ContinueBtn.UseVisualStyleBackColor = true;
            this.ContinueBtn.Click += new System.EventHandler(this.ContinueBtn_Click);
            // 
            // RatingButton
            // 
            this.RatingButton.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RatingButton.Location = new System.Drawing.Point(474, 99);
            this.RatingButton.Name = "RatingButton";
            this.RatingButton.Size = new System.Drawing.Size(78, 23);
            this.RatingButton.TabIndex = 9;
            this.RatingButton.Text = "Рейтинг";
            this.RatingButton.UseVisualStyleBackColor = true;
            this.RatingButton.Click += new System.EventHandler(this.RatingButton_Click);
            // 
            // Sudoku
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.ClientSize = new System.Drawing.Size(564, 456);
            this.Controls.Add(this.RatingButton);
            this.Controls.Add(this.ContinueBtn);
            this.Controls.Add(this.PauseScreen);
            this.Controls.Add(this.PauseBtn);
            this.Controls.Add(this.TimerBox);
            this.Controls.Add(this.Answer);
            this.Controls.Add(this.Hard);
            this.Controls.Add(this.Medium);
            this.Controls.Add(this.Easy);
            this.Controls.Add(this.Check);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Sudoku";
            this.Text = "Sudoku";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PauseScreen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Check;
        private System.Windows.Forms.Button Easy;
        private System.Windows.Forms.Button Medium;
        private System.Windows.Forms.Button Hard;
        private System.Windows.Forms.Button Answer;
        private System.Windows.Forms.TextBox TimerBox;
        private System.Windows.Forms.Button PauseBtn;
        private System.Windows.Forms.PictureBox PauseScreen;
        private System.Windows.Forms.Button ContinueBtn;
        private System.Windows.Forms.Button RatingButton;
    }
}

