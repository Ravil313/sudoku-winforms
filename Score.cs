﻿using Sudoku.DataAccsessLayer;
using Sudoku.DataAccsessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Sudoku
{
    public partial class Score : Form
    {
        private DataContext dataContext;
        public Score()
        {
            InitializeComponent();
        }

        private void Score_Load(object sender, EventArgs e)
        {
            dataContext = new DataContext();
            var easyScore = dataContext.PlayersScore
                .AsNoTracking()
                .ToList();
            List<PlayerScoreDataView> playerScoreDataViews = new List<PlayerScoreDataView>();
            foreach (var x in easyScore)
            {
                playerScoreDataViews.Add(new PlayerScoreDataView(x.DateTime, x.PlayerName, x.Time, x.Complexity));
            }
            easyScoreGridView.DataSource = playerScoreDataViews
                .Where(x => x.Complexity == "Легкая")
                .ToList();
            easyScoreGridView.Columns[0].HeaderText = "Дата и время игры";
            easyScoreGridView.Columns[1].HeaderText = "Имя игрока";
            easyScoreGridView.Columns[2].HeaderText = "Время игрока";
            easyScoreGridView.Columns[3].HeaderText = "Сложность";

            mediumScoreGridView.DataSource = playerScoreDataViews
                .Where(x => x.Complexity == "Средняя")
                .ToList();
            mediumScoreGridView.Columns[0].HeaderText = "Дата и время игры";
            mediumScoreGridView.Columns[1].HeaderText = "Имя игрока";
            mediumScoreGridView.Columns[2].HeaderText = "Время игрока";
            mediumScoreGridView.Columns[3].HeaderText = "Сложность";

            hardScoreGridView.DataSource = playerScoreDataViews
                .Where(x => x.Complexity == "Сложная")
                .ToList();
            hardScoreGridView.Columns[0].HeaderText = "Дата и время игры";
            hardScoreGridView.Columns[1].HeaderText = "Имя игрока";
            hardScoreGridView.Columns[2].HeaderText = "Время игрока";
            hardScoreGridView.Columns[3].HeaderText = "Сложность";
        }
    }
}
