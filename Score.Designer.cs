﻿namespace Sudoku
{
    partial class Score
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Score));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.EasyScore = new System.Windows.Forms.TabPage();
            this.easyScoreGridView = new System.Windows.Forms.DataGridView();
            this.MediumScore = new System.Windows.Forms.TabPage();
            this.HardScore = new System.Windows.Forms.TabPage();
            this.mediumScoreGridView = new System.Windows.Forms.DataGridView();
            this.hardScoreGridView = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.EasyScore.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.easyScoreGridView)).BeginInit();
            this.MediumScore.SuspendLayout();
            this.HardScore.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mediumScoreGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hardScoreGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.EasyScore);
            this.tabControl1.Controls.Add(this.MediumScore);
            this.tabControl1.Controls.Add(this.HardScore);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(564, 456);
            this.tabControl1.TabIndex = 1;
            // 
            // EasyScore
            // 
            this.EasyScore.Controls.Add(this.easyScoreGridView);
            this.EasyScore.Location = new System.Drawing.Point(4, 22);
            this.EasyScore.Name = "EasyScore";
            this.EasyScore.Padding = new System.Windows.Forms.Padding(3);
            this.EasyScore.Size = new System.Drawing.Size(556, 430);
            this.EasyScore.TabIndex = 0;
            this.EasyScore.Text = "Легкакя";
            this.EasyScore.UseVisualStyleBackColor = true;
            // 
            // easyScoreGridView
            // 
            this.easyScoreGridView.AllowUserToAddRows = false;
            this.easyScoreGridView.AllowUserToDeleteRows = false;
            this.easyScoreGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.easyScoreGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.easyScoreGridView.Location = new System.Drawing.Point(3, 3);
            this.easyScoreGridView.Name = "easyScoreGridView";
            this.easyScoreGridView.ReadOnly = true;
            this.easyScoreGridView.Size = new System.Drawing.Size(550, 424);
            this.easyScoreGridView.TabIndex = 0;
            // 
            // MediumScore
            // 
            this.MediumScore.Controls.Add(this.mediumScoreGridView);
            this.MediumScore.Location = new System.Drawing.Point(4, 22);
            this.MediumScore.Name = "MediumScore";
            this.MediumScore.Padding = new System.Windows.Forms.Padding(3);
            this.MediumScore.Size = new System.Drawing.Size(556, 430);
            this.MediumScore.TabIndex = 1;
            this.MediumScore.Text = "Средняя";
            this.MediumScore.UseVisualStyleBackColor = true;
            // 
            // HardScore
            // 
            this.HardScore.Controls.Add(this.hardScoreGridView);
            this.HardScore.Location = new System.Drawing.Point(4, 22);
            this.HardScore.Name = "HardScore";
            this.HardScore.Size = new System.Drawing.Size(556, 430);
            this.HardScore.TabIndex = 2;
            this.HardScore.Text = "Cложная";
            this.HardScore.UseVisualStyleBackColor = true;
            // 
            // mediumScoreGridView
            // 
            this.mediumScoreGridView.AllowUserToAddRows = false;
            this.mediumScoreGridView.AllowUserToDeleteRows = false;
            this.mediumScoreGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mediumScoreGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mediumScoreGridView.Location = new System.Drawing.Point(3, 3);
            this.mediumScoreGridView.Name = "mediumScoreGridView";
            this.mediumScoreGridView.ReadOnly = true;
            this.mediumScoreGridView.Size = new System.Drawing.Size(550, 424);
            this.mediumScoreGridView.TabIndex = 0;
            // 
            // hardScoreGridView
            // 
            this.hardScoreGridView.AllowUserToAddRows = false;
            this.hardScoreGridView.AllowUserToDeleteRows = false;
            this.hardScoreGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.hardScoreGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hardScoreGridView.Location = new System.Drawing.Point(0, 0);
            this.hardScoreGridView.Name = "hardScoreGridView";
            this.hardScoreGridView.ReadOnly = true;
            this.hardScoreGridView.Size = new System.Drawing.Size(556, 430);
            this.hardScoreGridView.TabIndex = 0;
            // 
            // Score
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 456);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Score";
            this.Text = "Score";
            this.Load += new System.EventHandler(this.Score_Load);
            this.tabControl1.ResumeLayout(false);
            this.EasyScore.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.easyScoreGridView)).EndInit();
            this.MediumScore.ResumeLayout(false);
            this.HardScore.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mediumScoreGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hardScoreGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage EasyScore;
        private System.Windows.Forms.TabPage MediumScore;
        private System.Windows.Forms.TabPage HardScore;
        private System.Windows.Forms.DataGridView easyScoreGridView;
        private System.Windows.Forms.DataGridView mediumScoreGridView;
        private System.Windows.Forms.DataGridView hardScoreGridView;
    }
}