﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku
{
    internal class CreateMap
    {
        private const int n = 9;
        private int[] map = new int[n*n];
        private Random random = new Random();
        public int[,] Create()
        {
            FillFirstRowRandomly();            
            var createdMap = new SolveByBacktraking(map);
            return createdMap.Solution();             
        }
        private void FillFirstRowRandomly()
        {
            List<int> numbers = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            for (int x = 0; x < n; x++)
            {
                int randElement = random.Next(numbers.Count);
                map[x] = numbers.ElementAt(randElement);
                numbers.RemoveAt(randElement);
            }
        }
    }
}
