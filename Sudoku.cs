﻿using System;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;
using Sudoku.DataAccsessLayer;
using Sudoku.DataAccsessLayer.Models;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace Sudoku
{
    public partial class Sudoku : Form
    {
        private const int n = 9;
        private int h, m, s, hideCells, buttonSize = 50;
        private int[,] _map = new int[n, n];
        private Button[,] _buttons = new Button[n, n];
        private System.Timers.Timer t;
        private DataContext dataContext;
        private string _complexity;
        private string _time;
        private int _score;
        public Sudoku()
        {
            InitializeComponent();
        }
        private void Easy_Click(object sender, EventArgs e)
        {
            Check.Enabled = true;
            Answer.Enabled = true;
            PauseBtn.Enabled = true;
            CreateMap();
            HideCells(40);
            RefreshTimer();
            t.Start();
            _complexity = "Легкая";
        }
        private void Medium_Click(object sender, EventArgs e)
        {            
            Check.Enabled = true;
            Answer.Enabled = true;
            PauseBtn.Enabled = true;
            CreateMap();
            HideCells(50);
            RefreshTimer();
            t.Start();
            _complexity = "Средняя";
        }
        private void Hard_Click(object sender, EventArgs e)
        {
            Check.Enabled = true;
            Answer.Enabled = true;
            PauseBtn.Enabled = true;
            CreateMap();
            HideCells(60);
            RefreshTimer();
            t.Start();
            _complexity = "Сложная";
        }
        private void CreateMap()
        {
            ClearMap();
            CreateMap createMap = new CreateMap();
            int[,] map = new int[n, n];
            map = createMap.Create();
            _map = map;
            GenerateMap(_map);
        }
        private async void Check_Click(object sender, EventArgs e)
        {
            for (int y = 0; y < n; y++)
            {
                for (int x = 0; x < n; x++)
                {
                    var btnText = _buttons[y, x].Text;
                    if (_buttons[y, x].Enabled)
                    {
                        if (btnText == _map[y, x].ToString())
                        {
                            _buttons[y, x].BackColor = Color.LimeGreen;
                            _buttons[y, x].Enabled = false;
                            hideCells--;
                        }
                        else
                        {
                            _buttons[y, x].BackColor = Color.Coral;
                        }
                    }
                }
                if (hideCells == 0)
                {
                    MessageBox.Show("Победа!");
                    Easy_Click(null, null);
                    t.Stop();
                    await SaveInDB();
                    return;
                }
            }
        }
        private async void Answer_Click(object sender, EventArgs e)
        {
            await SaveInDB();            
            PauseBtn.Enabled = false;
            ContinueBtn.Enabled = false;
            t.Stop();
            ClearMap();
            GenerateMap(_map);
        }
        private async Task SaveInDB()
        {
            await Task.Run(() =>
            {
                PlayerScoreData playerScore = new PlayerScoreData();
                playerScore.PlayerName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                playerScore.DateTime = DateTime.Now;
                playerScore.Time = _time;
                playerScore.Complexity = _complexity;
                playerScore.Score = _score;
                dataContext.PlayersScore.Add(playerScore);
                dataContext.SaveChanges();
            });
        }
        private void PauseBtn_Click(object sender, EventArgs e)
        {
            PauseScreen.Image = Properties.Resources._125;
            Easy.Enabled = false;
            Medium.Enabled = false;
            Hard.Enabled = false;
            Answer.Enabled = false;
            ContinueBtn.Enabled = true;
            PauseScreen.Visible = true;
            t.Stop();
            PauseBtn.Enabled = false;
        }
        private void ContinueBtn_Click(object sender, EventArgs e)
        {
            Easy.Enabled = true;
            Medium.Enabled = true;
            Hard.Enabled = true;
            Answer.Enabled = true;
            PauseBtn.Enabled = true;
            PauseScreen.Visible = false;
            t.Start();
            ContinueBtn.Enabled = false;
        }               
        private void GenerateMap(int[,] map)
        {
            var font = new Font("Arial", 16, FontStyle.Bold);
            int offsetX = 0;
            int offsetY = 0;
            for (int y = 0; y < n; y++)
            {
                if (y == 3 || y == 6)
                    offsetY += 3;
                for (int x = 0; x < n; x++)
                {
                    if (x == 3 || x == 6)
                        offsetX += 3;
                    var button = new Button();
                    button.Size = new Size(buttonSize, buttonSize);
                    button.Text = map[y, x].ToString();
                    button.Font = font;
                    button.Location = new Point(x * buttonSize + offsetX, y * buttonSize + offsetY);
                    button.Click += OnCellPressed;
                    button.BackColor = Color.White;
                    button.Enabled = false;
                    _buttons[y, x] = button;
                    Controls.Add(button);
                    if (x == 8)
                        offsetX = 0;
                }
            }
        }
        private void OnCellPressed(object sender, EventArgs e)
        {
            var pressedButton = (Button)sender;
            pressedButton.BackColor = Color.Yellow;
            var buttonText = pressedButton.Text;
            if (string.IsNullOrEmpty(buttonText))
                pressedButton.Text = "1";
            else
            {
                var num = int.Parse(buttonText);
                num++;
                if (num == 10)
                    num = 1;
                pressedButton.Text = num.ToString();
            }
        }
        private void HideCells(int complexity)
        {
            var r = new Random();
            while (complexity > 0)
            {
                var y = r.Next(0, n);
                var x = r.Next(0, n);
                if (!_buttons[y, x].Enabled)
                {
                    hideCells += 1;
                    _buttons[y, x].Text = "";
                    _buttons[y, x].Enabled = true;
                    complexity--;
                }
            }
        }
        private void ClearMap()
        {
            for (int y = 0; y < n; y++)
            {
                for (int x = 0; x < n; x++)
                {
                    Controls.Remove(_buttons[y, x]);
                }
            }
        }

        private void RatingButton_Click(object sender, EventArgs e)
        {
            Score scorePanel = new Score();
            scorePanel.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            t = new System.Timers.Timer();
            t.Interval = 1000;
            t.Elapsed += OnTimeEvent;
            dataContext = new DataContext();
            Easy_Click(null, null);
        }
        private void OnTimeEvent(object sender, ElapsedEventArgs e)
        {
            int scoreH = 0;
            Invoke(new Action(() =>
            {
                s += 1;
                _score += 1;
                if (s == 60)
                {
                    s = 0;
                    m += 1;
                    _score = m * 100 + scoreH;
                }
                if (m == 60)
                {
                    m = 0;
                    h += 1;
                    scoreH = h * 10000;
                    _score = scoreH;
                }
                _time = string.Format("{0} : {1} : {2}", h.ToString().PadLeft(2, '0'), m.ToString().PadLeft(2, '0'), s.ToString().PadLeft(2, '0'));
                TimerBox.Text = _time;
            }));
        }
        private void RefreshTimer()
        {
            h = 0;
            m = 0;
            s = 0;
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            t.Stop();
            Application.DoEvents();
            SqlConnection.ClearAllPools();
        }
    }
}
