﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku
{
    internal class SolveByBacktraking
    {
        private readonly int[] grid;
        public SolveByBacktraking(int[] task)
        {
            grid = new int[task.Length];
            grid = task;
        }
        private void Solve()
        {
            try
            {
                PlaceNumber(0);
                Console.WriteLine("Unsolvable!");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(this);
            }
        }
        private void PlaceNumber(int pos)
        {
            if (pos == 81)
            {
                throw new Exception("Finished!");
            }
            if (grid[pos] > 0)
            {
                PlaceNumber(pos + 1);
                return;
            }
            for (int n = 1; n <= 9; n++)
            {
                if (CheckValidity(n, pos % 9, pos / 9))
                {
                    grid[pos] = n;
                    PlaceNumber(pos + 1);
                    grid[pos] = 0;
                }
            }
        }
        private bool CheckValidity(int val, int x, int y)
        {
            for (int i = 0; i < 9; i++)
            {
                if (grid[y * 9 + i] == val || grid[i * 9 + x] == val)
                    return false;
            }
            int startX = (x / 3) * 3;
            int startY = (y / 3) * 3;
            for (int i = startY; i < startY + 3; i++)
            {
                for (int j = startX; j < startX + 3; j++)
                {
                    if (grid[i * 9 + j] == val)
                        return false;
                }
            }
            return true;
        }
        public int [,] Solution()
        {
            Solve();
            int [,] sol = new int[9,9];
            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    sol[y,x] = grid[y * 9 + x];
                }
            }
            return sol;
        }
    }
}
