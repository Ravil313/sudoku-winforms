﻿using System;

namespace Sudoku.DataAccsessLayer.Models
{
    internal class PlayerScoreData
    {
        public int Id {  get; set; }
        public DateTime DateTime { get; set; }
        public string PlayerName { get; set; }
        public string Time { get; set; }
        public string Complexity { get; set; } 
        public int Score { get; set; }
    }
}
