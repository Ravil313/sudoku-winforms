﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku.DataAccsessLayer.Models
{
    internal class PlayerScoreDataView
    {
        public PlayerScoreDataView(DateTime raiting, string playerName, string time, string complexity)
        {
            DateTime = raiting;
            PlayerName = playerName;
            Time = time;
            Complexity = complexity;
        }
        public DateTime DateTime { get; set; }
        public string PlayerName { get; set; }
        public string Time { get; set; }
        public string Complexity { get; set; }
    }
}
