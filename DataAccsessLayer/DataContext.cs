﻿using System.Data.Entity;
using Sudoku.DataAccsessLayer.Models;

namespace Sudoku.DataAccsessLayer
{
    internal class DataContext : DbContext
    {
        public DbSet<PlayerScoreData> PlayersScore { get; set; }
        public DataContext() : base("Database1") { }
    }
}
